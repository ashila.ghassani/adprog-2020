package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int value = weapon.getWeaponValue();
        int randomInt = ThreadLocalRandom.current().nextInt(1, 6);
        value += randomInt;
        return value;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription()+" - Upgrade with Regular -";
    }
}
