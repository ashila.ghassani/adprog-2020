package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;
    private Member master;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
        master = new PremiumMember("Heathcliff", "Master");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Aqua",member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Goddess",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member child = new PremiumMember("child","child");
        member.addChildMember(child);
        assertEquals(1,member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member child = new PremiumMember("child","child");
        member.addChildMember(child);
        assertEquals(1,member.getChildMembers().size());
        member.removeChildMember(child);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child = new PremiumMember("child","child");
        member.addChildMember(child);
        Member child2 = new PremiumMember("child2","child");
        member.addChildMember(child2);
        Member child3 = new PremiumMember("child3","child");
        member.addChildMember(child3);
        Member child4 = new PremiumMember("child4","child");
        member.addChildMember(child4);
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child = new PremiumMember("child","Master");
        master.addChildMember(child);
        Member child2 = new PremiumMember("child2","Master");
        master.addChildMember(child2);
        Member child3 = new PremiumMember("child3","Master");
        master.addChildMember(child3);
        Member child4 = new PremiumMember("child4","Master");
        master.addChildMember(child4);
        assertEquals(4,master.getChildMembers().size());
    }
}
