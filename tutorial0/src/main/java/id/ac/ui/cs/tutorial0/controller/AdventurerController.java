package id.ac.ui.cs.tutorial0.controller;

import id.ac.ui.cs.tutorial0.service.AdventurerCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdventurerController {

    @Autowired
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
        int powerNum = adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear);
        model.addAttribute("power", powerNum);

        String addTeks= null;

        if(powerNum <= 20000){
            addTeks = "C Class";
        }
        else if(powerNum<=100000 && powerNum>20000){
            addTeks = "B Class";
        }
        else {
            addTeks = "A Class";
        }
        model.addAttribute("level",addTeks);

        return "calculator";
    }
}
